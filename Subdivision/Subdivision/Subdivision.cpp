#include <fstream>
#include <vector>

#include "OBJFileReader.h"
#include "Solid.h"
#include "iterators.h"

using namespace MeshLib;

void print_stats(Solid& mesh);

void main(int argc, char *argv[])
{
	// Read in the obj file
	Solid mesh1, mesh;
	OBJFileReader of;
	std::ifstream in(argv[1]);
	of.readToSolid(&mesh1, in);
	mesh1.copy(mesh);

	/******************* Put you subdivision processing here *********************/
	std::cout << "Original Mesh Stats : \n";
	print_stats(mesh);

	// Copy the original edge list
	SolidEdgeIterator edgeIter(&mesh);
	std::vector<Edge*>  origEdgeList;
	for (; !edgeIter.end(); ++edgeIter)
	{
		(*edgeIter)->string() = "black";  // black = old edges, blue = new edges
		origEdgeList.push_back(*edgeIter);
	}

	// Compute the edge midpoint. To replace with actual formula
	std::vector<Vertex*> edgeMidPoint;
	edgeIter.reset();
	for (; !edgeIter.end(); ++edgeIter)
	{
		Vertex *v1, *v2, *v_new = new Vertex(); // TODO: remove heap allocation for v_new()
		(*edgeIter)->get_vertices(v1, v2);
		v_new->point() = (v1->point() + v2->point()) / 2;
		v1->string() = "old"; v2->string() = "old";
		edgeMidPoint.push_back(v_new);
	}

	// Perform edge split and update each new vertex with the edgeMidpoint computed above
	std::vector<Edge*>::iterator  origEdgeListIter = origEdgeList.begin(); // nextOrigEdgeListIter;
	std::vector<Vertex*>::iterator edgeMidPointIter = edgeMidPoint.begin();
	Edge* curr_edge;
	while (1)
	{
		if (origEdgeListIter != origEdgeList.end()) {
			curr_edge = *origEdgeListIter; ++origEdgeListIter;
		}
		else break;
		Vertex* v1, * v2;
		curr_edge->get_vertices(v1, v2);
		Vertex* newVert = mesh.edgeSplit(curr_edge);
		newVert->point() = (*edgeMidPointIter)->point();
		newVert->string() = "new";
		++edgeMidPointIter;

		SolidEdgeIterator meshIter1(&mesh);
		for (; !meshIter1.end(); ++meshIter1)
		{
			Edge* e = *meshIter1;
			if (e->string() != "blue" && e->string() != "black")
			{
				if (e->include_vertex(v1) || e->include_vertex(v2)) {
					e->string() = "black";
				}
				else e->string() = "blue";
			}
		}

	}

	// Break old vertex to new vertex edges and connect the corresponding "perpendicular vertices"
	std::vector<Edge*>  edgeToDelete;
	SolidEdgeIterator edgeIter2(&mesh);
	std::vector<std::tuple<Vertex*, Vertex*, Vertex*, Vertex*>> edgesToSwap;
	std::tuple<Vertex*, Vertex*, Vertex*, Vertex*> temp_tuple;
	for (; !edgeIter2.end(); ++edgeIter2)
	{
		Vertex* v1, * v2, *v3, *v4;
		(*edgeIter2)->get_vertices(v1, v2);
		if (v1->string() != v2->string() && (*edgeIter2)->string() == "blue" )
		{
			edgeToDelete.push_back(*edgeIter2);
			// find the 'perpendicular' vertices v3, v4
			// TODO : Check for boundary case here for the edges
			HalfEdge* hfe1 = (*edgeIter2)->halfedge(0), * hfe2 = (*edgeIter2)->halfedge(1);
			v3 = (hfe1->he_next())->vertex();
			v4 = (hfe2->he_next())->vertex();
			temp_tuple = std::make_tuple(v1, v2, v3, v4);
			edgesToSwap.push_back(temp_tuple);
		}
	}

	std::cout << "\nUpdated Mesh Stats : \n";
	print_stats(mesh);

	/******************************************************************************/

	// Write out the resultant obj file
	int vObjID = 1;
	std::map<int, int> vidToObjID;
	std::cout << "argv[2] = " << argv[2] << "\n";
	std::ofstream os(argv[2]);

	SolidVertexIterator iter(&mesh);

	for(; !iter.end(); ++iter)
	{
		Vertex *v = *iter;
		Point p = v->point();
		os << "v " << p[0] << " " << p[1] << " " << p[2] << std::endl;
		vidToObjID[v->id()] = vObjID++;
	}
	os << "# " << (unsigned int)mesh.numVertices() << " vertices" << std::endl;

	float u = 0.0, v = 0.0;
	for(iter.reset(); !iter.end(); ++iter)
	{
		Vertex *vv = *iter;
		std::string key( "uv" );
		std::string s = Trait::getTraitValue (vv->string(), key );
		if( s.length() > 0 )
		{
			sscanf( s.c_str (), "%f %f", &u, &v );
		}
		os << "vt " << u << " " << v << std::endl;
	}
	os << "# " << (unsigned int)mesh.numVertices() << " texture coordinates" << std::endl;

	SolidFaceIterator fiter(&mesh);
	for(; !fiter.end(); ++fiter)
	{
		Face *f = *fiter;
		FaceVertexIterator viter(f);
		os << "f " ;
		for(; !viter.end(); ++viter)
		{
			Vertex *v = *viter;
			os << vidToObjID[v->id()] << "/" << vidToObjID[v->id()] << " ";
		}
		os << std::endl;
	}
	os.close();
}

void print_stats(Solid& mesh)
{
	SolidHalfEdgeIterator halfEdgeIter(&mesh); int count = 0;
	for (; !halfEdgeIter.end(); ++halfEdgeIter)
	{
		HalfEdge* v = *halfEdgeIter;
		count++;
	}
	std::cout << "Half-edge count = " << count << std::endl;

	SolidEdgeIterator edgeIter(&mesh); count = 0;
	for (; !edgeIter.end(); ++edgeIter)
	{
		Edge* v = *edgeIter;
		count++;
	}
	std::cout << "Edge count = " << count << std::endl;

	SolidVertexIterator vertIter(&mesh); count = 0;
	for (; !vertIter.end(); ++vertIter)
	{
		Vertex* v = *vertIter;
		std::cout << v->id() <<" ";
		count++;
	}
	std::cout << "Vertex count = " << count << std::endl;

	SolidFaceIterator faceIter(&mesh); count = 0;
	for (; !faceIter.end(); ++faceIter)
	{
		Face* v = *faceIter;
		count++;
	}
	std::cout << "Face count = " << count << std::endl;

}